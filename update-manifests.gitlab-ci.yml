include:
  - local: .gitlab/ci_templates/git-push.yaml
  # - template: Security/SAST-IaC.latest.gitlab-ci.yml

variables:
 SCAN_KUBERNETES_MANIFESTS: "true"
 KUBESEC_HELM_CHARTS_PATH: $CI_PROJECT_DIR/constructed-manifests/

stages:
  - build
  - test
  - update_staging_manifests
  - update_production_manifests

construct-staging-manifests:
  extends: .hydrate-package
  stage: build
  variables:
    TARGET_ENVIRONMENT_NAME: staging
  rules:
    - if: '$CD_STRATEGY != "deliver-to-staging"'
      when: never
    - if: '$VERSIONUPDATE == "true" && $CD_STRATEGY == "deliver-to-staging"'
    - if: '$GIT_PUSH_OCCURRED == "true" && $CD_STRATEGY == "deliver-to-staging"'

construct-production-manifests:
  extends: .hydrate-package
  stage: build
  variables:
    TARGET_ENVIRONMENT_NAME: production  
  rules:
    - if: '$CD_STRATEGY != "deliver-to-staging"'
      when: never
    - if: '$VERSIONUPDATE == "true" && $CD_STRATEGY == "deliver-to-staging"'
    - if: '$GIT_PUSH_OCCURRED == "true" && $CD_STRATEGY == "deliver-to-staging"'

update-staging-manifests:
  extends: .git:push  
  stage: update_staging_manifests
  image: 
    name: bash
    entrypoint: [""]  
  variables:
    SKIP_CI: 1
    COMMIT_MESSAGE: "staging: $COMMIT_MESSAGE"
  environment: 
    name: staging
    url: "https://${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_ENVIRONMENT_NAME}.${KUBE_INGRESS_BASE_DOMAIN}"
  rules:
    - if: '$CD_STRATEGY != "deliver-to-staging"'
      when: never
    - if: '$VERSIONUPDATE == "true" && $CD_STRATEGY == "deliver-to-staging"'
    - if: '$GIT_PUSH_OCCURRED == "true" && $CD_STRATEGY == "deliver-to-staging"'
  script:
    - |    
      mkdir -p "${CI_COMMIT_SHA}/manifests"
      rm -f "${CI_COMMIT_SHA}/manifests/${SERVICE_NAME}.${CI_ENVIRONMENT_NAME}.yaml"
      cp -r constructed-manifests/${SERVICE_NAME}.${CI_ENVIRONMENT_NAME}.yaml "${CI_COMMIT_SHA}/manifests/"

update-production-manifests:
  extends: .git:push
  stage: update_production_manifests
  variables:
    SKIP_CI: 1
    COMMIT_MESSAGE: "production: $COMMIT_MESSAGE"
  environment: 
      name: production
      url: "https://${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_ENVIRONMENT_NAME}.${KUBE_INGRESS_BASE_DOMAIN}"
  rules:
    - if: '$GIT_PUSH_OCCURRED == "true" && $CD_STRATEGY != "deploy-to-production"'
      when: manual
    - if: '$VERSIONUPDATE == "true" && $CD_STRATEGY != "deploy-to-production"'
      when: manual
    - if: '$GIT_PUSH_OCCURRED == "true" && $CD_STRATEGY == "deploy-to-production"'
    - if: '$VERSIONUPDATE == "true" && $CD_STRATEGY == "deploy-to-production"'
  script:
    - |    
      mkdir -p "${CI_COMMIT_SHA}/manifests"
      rm -f "${CI_COMMIT_SHA}/manifests/${SERVICE_NAME}.${CI_ENVIRONMENT_NAME}.yaml"
      cp -r constructed-manifests/${SERVICE_NAME}.${CI_ENVIRONMENT_NAME}.yaml "${CI_COMMIT_SHA}/manifests/"    

.hydrate-package:
  image: 
    name: bash
      #line/kubectl-kustomize
    entrypoint: [""]
  script:
    - |
      #set -xv
      echo "Proceeding with CD_STRATEGY '$CD_STRATEGY' for ${TARGET_ENVIRONMENT_NAME} environment by updating manifest file 'manifests/${SERVICE_NAME}.${TARGET_ENVIRONMENT_NAME}.yaml'"
      apk add --quiet curl
      wget -O - "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
      mv kustomize /usr/bin/kustomize
      echo "Creating kustomization.yaml"
      echo "NEXTVERSION=${NEXTVERSION}"
      rm -rf manifests
      mkdir -p manifests
      # cp packages/*.yaml manifests/

      # Create Routes for OCP demo
      echo "Creating routes ..."

      cat <<EOF-DYNINGRESS >./packages/${SERVICE_NAME}/${TARGET_ENVIRONMENT_NAME}/dynamicroutes.yaml
      apiVersion: route.openshift.io/v1
      kind: Route
      metadata:
        name: ${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${TARGET_ENVIRONMENT_NAME}
        namespace: ${CI_PROJECT_NAME}-${TARGET_ENVIRONMENT_NAME}
      spec:
        host: ${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${TARGET_ENVIRONMENT_NAME}.${KUBE_INGRESS_BASE_DOMAIN}
        port:
          targetPort: http
        to:
          kind: Service
          name: ${SERVICE_NAME}
      EOF-DYNINGRESS


      cat <<EOF-DYNSERVICE >./packages/${SERVICE_NAME}/${TARGET_ENVIRONMENT_NAME}/service.yaml
      apiVersion: v1
      kind: Service
      metadata:
        name: ${SERVICE_NAME}
        namespace: ${CI_PROJECT_NAME}-${TARGET_ENVIRONMENT_NAME}
      spec:
        ports:
        - port: 80
          targetPort: 5000
          protocol: TCP
          name: http
        selector:
          app: ${SERVICE_NAME}
          environment: ${TARGET_ENVIRONMENT_NAME}
      EOF-DYNSERVICE

      cat <<EOF-KUSTOMIZE >./packages/${SERVICE_NAME}/${TARGET_ENVIRONMENT_NAME}/kustomization.yaml
      apiVersion: kustomize.config.k8s.io/v1beta1
      kind: Kustomization
      # namespace: ${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${TARGET_ENVIRONMENT_NAME}
      namespace: ${CI_PROJECT_NAME}-${TARGET_ENVIRONMENT_NAME}
      commonLabels:
        app: ${SERVICE_NAME}
        environment: ${TARGET_ENVIRONMENT_NAME}
      resources:
      - "../base"
      - dynamicroutes.yaml
      - namespace.yaml
      - service.yaml
      images: 
      - name: _replace-with-hello-world-service-container-registry-path_
        newName: ${IMAGE_NAME_TO_MONITOR}
        newTag: ${NEXTVERSION}
      EOF-KUSTOMIZE
      echo "Showing resultant kustomization.yaml"
      cat ./packages/${SERVICE_NAME}/${TARGET_ENVIRONMENT_NAME}/kustomization.yaml
      mkdir -p constructed-manifests
      kustomize build packages/${SERVICE_NAME}/${TARGET_ENVIRONMENT_NAME} > constructed-manifests/${SERVICE_NAME}.${TARGET_ENVIRONMENT_NAME}.yaml
      cat constructed-manifests/${SERVICE_NAME}.${TARGET_ENVIRONMENT_NAME}.yaml
    
  artifacts:
    untracked: false
    expire_in: 1 days
    expose_as: Updated-Manifests
    paths:
      - constructed-manifests/${SERVICE_NAME}.${TARGET_ENVIRONMENT_NAME}.yaml
